########################################################
# Makefile 
# ----------------------------------------------------------------------
#
# ----------------------------------------------------------------------
# History
#   2019-02-01 Initial version
#

.DEFAULT_GOAL := help
.PHONY: list

help:
	@echo "ProyectName - Makefile"
	@echo " - make cmd01           desc01"
	@echo " - make cmd02           desc02"
	@echo " - make cmd03           desc03"
	@echo " - ejecutar make list para ver los targets diponibles"
	@echo "Targets:"
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1}}' | \
	sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$' |  column -x -c 180  | sed -e 's/^/ /'

list:
	@$(MAKE) -pRrq -f $(lastword $(MAKEFILE_LIST)) : 2>/dev/null | awk -v RS= -F: '/^# File/,/^# Finished Make data base/ {if ($$1 !~ "^[#.]") {print $$1 }}' | \
        sort | egrep -v -e '^[^[:alnum:]]' -e '^$@$$'

echo:
	echo "hostname ${HOSTNAME}"

getChromeTab:
	ruby tabs.rb > etc/chrome-tabs.lst

	
getData: 
	echo "#getData"
	git pull origin master
	curl -s https://todoist.com/api/v8/sync -d token="$$TODOIST_TOKEN" -d sync_token=* -d resource_types='["all"]' | python -m json.tool > etc/todoist.all.json
	php openTodoistItems.php
	git add data/* etc/todoist.all.json
	git add CHANGELOG.md
	git commit -m "Auto-update"
	#git push origin master

listIssues:
	@for number in 10533270 ; do \
		name=$$( curl --silent --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$$number" | jq '.name' | sed -e 's/^"//' -e 's/"$$//') ; \
		echo "pid=$$number - $$name" ; \
		curl --silent --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$$number/issues?state=opened" | jq '.[] | .iid,.title' | sed -e 's/^"//' -e 's/"$$//' | sed -e 's/^/  / ' ; \
	done

listAllIssues:
	@for number in $$( cat etc/projects-id.lst ) ; do \
		name=$$( curl --silent --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$$number" | jq '.name' | sed -e 's/^"//' -e 's/"$$//') ; \
		echo "=================================" ; \
		echo "pid=$$number - $$name" ; \
		curl --silent --header "PRIVATE-TOKEN: ${GITLAB_TOKEN}" "https://gitlab.com/api/v4/projects/$$number/issues?state=opened" | jq '.[] | .iid,.title' | sed -e 's/^"//' -e 's/"$$//' | sed -e 's/^/  / ' ; \
	done

changeLog:
	changeLog  > CHANGELOG.md




checkESJenkins:
	curl -s -XGET "http://localhost:9200/_cat/indices/jenkins*?s=index&format=json" | jq "."