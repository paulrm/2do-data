<?PHP

    #
    # Valores iniciales
    #
    $outdir="data/";
    $i=0;
    $newFiles = array();
    #
    # Cargo los files de dir $outdit
    #
    #echo "scandir\n";
    #print_r(scandir($outdir));
    $files = array_diff(scandir($outdir), array('.', '..', 'todoist.all.json'));

    #
    # Toma los datos del archivo json y los deja en array
    #
    $data=file_get_contents("etc/todoist.all.json") ;
    $decoded=json_decode($data, true);
    #print_r($decoded);

    #
    # Loop a los largo de los items
    #
    foreach($decoded["items"] as $v)
        {
        ksort($v);
        $out=json_encode($v,JSON_PRETTY_PRINT);
        $fname=$outdir . "/" . $v["id"] . ".json";
        $newFiles[]=$v["id"]. ".json";
        file_put_contents($fname,$out);
        #echo "writed id=" . $v["id"] . "\n";
        $i++;
        }
    #
    #  End process
    #
    printf( "%d Items in data dir\n", count($files));
    printf( "%d Items processed\n", $i);

//    echo "Files\n";
//    print_r($files);
//
//    echo "Files\n";
//    print_r($newFiles);

    echo "Files to move:\n";
    $result = array_diff($files, $newFiles);
    #print_r($result);
    foreach($result as $v)
       {
	printf("git rm data/%s\n", $v);
       }
